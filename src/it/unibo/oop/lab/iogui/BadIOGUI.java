/**
 * 
 */
package it.unibo.oop.lab.iogui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.im.InputContext;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Random;

import javax.print.attribute.standard.OutputDeviceAssigned;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.omg.CORBA.portable.OutputStream;

/**
 * This class is a simple application that writes a random number on a file.
 * 
 * This application does not exploit the model-view-controller pattern, and as
 * such is just to be used to learn the basics, not as a template for your
 * applications.
 */
public class BadIOGUI {

    private static final String TITLE = "A very simple GUI application";
    private static final String PATH = System.getProperty("user.home") + System.getProperty("file.separator")
            + BadIOGUI.class.getSimpleName() + ".txt";

    private static final int PROPORTION = 5;
    private final Random rng = new Random();
    private final JFrame frame = new JFrame(TITLE);

    /**
     * 
     */
    public BadIOGUI() {
        final JPanel canvas = new JPanel();
        final JPanel exercise0101 = new JPanel();
        canvas.setLayout(new BorderLayout());
        exercise0101.setLayout(new BoxLayout(exercise0101, BoxLayout.X_AXIS));
        final JButton write = new JButton("Write on file");
        final JButton read = new JButton("read");
        exercise0101.add(write);
        exercise0101.add(read);
        canvas.add(exercise0101, BorderLayout.CENTER);
        frame.setContentPane(canvas);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        /*
         * Handlers
         */
        write.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                /*
                 * This would be VERY BAD in a real application.
                 * 
                 * This makes the Event Dispatch Thread (EDT) work on an I/O operation. I/O
                 * operations may take a long time, during which your UI becomes completely
                 * unresponsive.
                 */
                try (DataOutputStream ps = new DataOutputStream(new FileOutputStream(PATH))) {
                    final int n = rng.nextInt();
                    ps.writeInt(n);
                    System.out.println(Integer.toString(n));
                } catch (Exception e1) {
                    JOptionPane.showMessageDialog(frame, e1, "Error", JOptionPane.ERROR_MESSAGE);
                    e1.printStackTrace();
                }
            }
        });

        read.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                try (DataInputStream rs = new DataInputStream(new FileInputStream(PATH))) {
                    System.out.println(rs.readInt());
                } catch (Exception e1) {
                    JOptionPane.showMessageDialog(frame, e1, "Error", JOptionPane.ERROR_MESSAGE);
                }
            }

        });
    }

    private void display() {
        /*
         * Make the frame one fifth the resolution of the screen. This very method is
         * enough for a single screen setup. In case of multiple monitors, the primary
         * is selected. In order to deal coherently with multimonitor setups, other
         * facilities exist (see the Java documentation about this issue). It is MUCH
         * better than manually specify the size of a window in pixel: it takes into
         * account the current resolution.
         */
        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        frame.setSize(sw / PROPORTION, sh / PROPORTION);
        /*
         * Instead of appearing at (0,0), upper left corner of the screen, this flag
         * makes the OS window manager take care of the default positioning on screen.
         * Results may vary, but it is generally the best choice.
         */
        frame.setLocationByPlatform(true);
        frame.pack();
        /*
         * OK, ready to pull the frame onscreen
         */
        frame.setVisible(true);
    }

    /**
     * @param args
     *            ignored
     */
    public static void main(final String... args) {
        // System.out.println(PATH);
        new BadIOGUI().display();
    }
}